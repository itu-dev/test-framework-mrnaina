/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Other/SQLTemplate.sql to edit this template
 */
/**
 * Author:  caeyla
 * Created: Nov 18, 2022
 */
create table admin(
    nom varchar(255),
    pseudo varchar(255) unique,
    motdepasse varchar(255)
);
insert into admin values(
    'Randria',
    'ndrina',
    '1234'
);
create table users(
    iduser bigserial primary key,
    nom varchar(255),
    pseudo varchar(255) unique,
    motdepasse varchar(255)
);
insert into users(nom,pseudo,motdepasse) values('Caeyla','cail','1234');
insert into users(nom,pseudo,motdepasse) values('Jean','doe','1234');

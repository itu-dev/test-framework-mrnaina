
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="css/style.css"/>
    <title>Acceuil</title>
  </head>
  <body>
    <div class="container pt-4">
      <div class="row pt-4 d-flex justify-content-center">
            <div class="col col-md-4 col-sm-12 form shadow-sm border">
              <div class="row ">
                <div class="col col-md-12 mt-2">
                  <p class="titre">SE CONNECTER</p>
                </div>
              </div>
                <p class="text-danger">
                    <%if(request.getAttribute("error")!=null){
                           out.print(request.getAttribute("error"));
                        }
                    %>
                </p>
               
              <div class="login-wrap">
                <form class="signin-form"  action="login.do" method="post">
                  <div class="form-group mt-1">
                    <input
                      type="text"
                      name="pseudo"
                      class="form-control"
                      required
                    />
                    <label class="form-control-placeholder" for="username"
                      >Identifiant</label
                    >
                  </div>
                  <div class="form-group">
                    <input
                      id="password-field"
                      type="password"
                      name="motdepasse"
                      class="form-control"
                      required
                    />
                    <label class="form-control-placeholder" for="password"
                      >Mot de passe</label
                    >
                  </div>
                  <div class="form-group">
                    <button
                      type="submit"
                      class="form-control btn-primary rounded submit px-3"
                    >
                      Se connecter
                    </button>
                  </div>
                </form>
              </div>
            </div>
        </div>
      </div>
  </body>
</html>
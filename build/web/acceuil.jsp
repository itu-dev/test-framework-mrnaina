<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="model.User,java.util.ArrayList" %>
 <% ArrayList<User> user=(ArrayList<User>)request.getAttribute("listeUser"); %>
 <!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/style.css" />
        <title>JSP Page</title>
    </head>
    <body>
       <body style="background-color: #f3f5f4;">
    <div class="container">
       

        <div class="row ">
            <div class=" col col-md-10">
                <h1>Liste des utilisateurs </h1>
                <table class="table pl-2 shadow-sm table-striped" style="overflow-y: scroll;">
                    <thead class="thead-dark">
                        <tr>
                            <th>Nom</th>
                           <th>Pseudo</th>
                            <th>Motdepasse</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                       
                        <% for(int i=0;i<user.size();i++) {
                        %>
                            <tr>
                                    <td class="w-40"><input type="text" name="nom" value="<% out.print(user.get(i).getNom()); %>"></td>
                                    <td><input type="text" name="pseudo" value="<% out.print(user.get(i).getPseudo()); %>"></td>
                                    <td><input type="text" name="motdepasse" value="<% out.print(user.get(i).getMotdepasse());%>"></td>
                            </tr>
                            <% }
                        %>
                    </tbody>
                </table>
            </div>
        </div>
    </body>
</html>

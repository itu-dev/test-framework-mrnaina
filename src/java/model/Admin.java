package model;

import dao.AdminDao;
import dao.UserDao;
import framework.ModelView;
import framework.annot.UrlAnnotation;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author caeyla
 */
public class Admin {
    String nom;
    String pseudo;
    String motdepasse;
    
    @UrlAnnotation(url = "login")
    public ModelView login(){
        ModelView mdlview=new ModelView();
        HashMap<String,Object> data=new HashMap<>();
        if(verifyLogin()!=null){
            mdlview.setUrl("acceuil.jsp");
            try {
                data.put("listeUser", UserDao.listUser());
            } catch (SQLException ex) {
                Logger.getLogger(Admin.class.getName()).log(Level.SEVERE, null, ex);
            }
        }else{
            mdlview.setUrl("index.jsp");
            data.put("error", "mot de passe ou pseudo incorrect");
        }
        mdlview.setData(data);
        return mdlview;
    }
    public Admin verifyLogin(){
        Admin ad=null;
        try{
           ad=AdminDao.authAdmin(this);
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
        return ad;
    }
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getMotdepasse() {
        return motdepasse;
    }

    public void setMotdepasse(String motdepasse) {
        this.motdepasse = motdepasse;
    }

    
    
}

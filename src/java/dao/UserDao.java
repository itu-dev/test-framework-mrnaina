/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.User;

/**
 *
 * @author caeyla
 */
public class UserDao {
    public static ArrayList<User> listUser() throws SQLException{
        Connection connection =null;
        PreparedStatement stm=null;
        ResultSet rslt=null;
        ArrayList<User> listUser=null;
        User usr=null;
        try {
            connection = ConnectionDeliver.getConnection();
            stm = connection.prepareStatement("Select * from users");
            rslt=stm.executeQuery();
            listUser=new ArrayList<>();
            while(rslt.next()){
                usr=new User();
                usr.setIdUser(rslt.getInt(1));
                usr.setNom(rslt.getString(2));
                usr.setPseudo(rslt.getString(3));
                usr.setMotdepasse(rslt.getString(4));
                listUser.add(usr);
            }
        } catch (SQLException ex) {
            throw ex;
        }finally{
            if(rslt!=null)rslt.close();
            if(stm!=null)stm.close();
            if(connection!=null)connection.close();
        }
        return   listUser;
    }

}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.Admin;

/**
 *
 * @author caeyla
 */
public class AdminDao {
    public static Admin authAdmin(Admin ad) throws SQLException {
        Connection connection =null;
        PreparedStatement stm=null;
        ResultSet rslt=null;
        try {
            connection = ConnectionDeliver.getConnection();
            stm = connection.prepareStatement("Select * from admin where pseudo=? and motdepasse=?");
            stm.setString(1, ad.getPseudo());
            stm.setString(2, ad.getMotdepasse());
            rslt=stm.executeQuery();
            if(rslt.next()){
                ad.setNom(rslt.getString(1));
                return ad;
            }
        } catch (SQLException ex) {
            throw ex;
        }finally{
            if(rslt!=null)rslt.close();
            if(stm!=null)stm.close();
            if(connection!=null)connection.close();
        }
        return null;
    }
}
